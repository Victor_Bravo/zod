import { loginSchema } from "./../schemas/auth.schema";
import { schemaValidation } from "./../middlewares/schemaValidate.middlewares";
import { login } from "./../controllers/auth.controllers";
import { Router } from "express";

const router = Router();

router.post("/login", schemaValidation(loginSchema), login);

export default router;
