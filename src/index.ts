import express from "express";
import authRouter from "./routes/auth.routes";

const app = express();
app.use(express.json());
app.use(authRouter);
app.listen(3000);
console.log(`Server on Port ${3000}`);
