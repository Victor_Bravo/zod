import { z } from "zod";

export const loginSchema = z.object({
  email: z.string().nonempty("email is require").email(),
  password: z.string().nonempty("password is required").min(6),
});
